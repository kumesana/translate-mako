package org.kumesana.mako.translate;

import org.kumesana.env.KumeEnv;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ReadFile {
    public static void main(String[] args) throws IOException {
        Path filePath = KumeEnv.getGamesDirectory().resolve("touch-mako/Mako Translation/data/patch/scenario/g5s00.ks");

        List<String> lines = Files.readAllLines(filePath, StandardCharsets.UTF_16LE);
        lines.forEach(System.out::println);
    }
}
