package org.kumesana.mako.translate;

import org.kumesana.common.strings.KStrings;
import org.kumesana.mako.translate.xml.XmlInlineContentReceiver;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LinkComponent implements LineComponent {
    private final Target target;
    private final String text;
    private final String builtString;

    public LinkComponent(Target target, String text) {
        this.target = target;
        this.text = text;
        builtString = "[link target=" + '"' + target.getTargetText() + '"' + "]" + text + "[endlink]";
    }

    public static LinkComponent buildFrom(String targetName, String text) {
        return new LinkComponent(Target.fromTargetName(targetName), text);
    }

    public Target getTarget() {
        return target;
    }

    public String getText() {
        return text;
    }

    @Override
    public String buildString() {
        return builtString;
    }

    @Override
    public void addToInlineReceiver(XmlInlineContentReceiver receiver) {
        receiver.addLink(target.getTargetName(), text);
    }

    public enum Target {
        OK("*確認ok"),
        CANCEL("*確認cancel"),
        DEFAULT("*確認default");

        private static final Map<String, Target> NAME_MAP = buildNameMap();

        private final String targetText;
        private final String targetName;

        Target(String targetText) {
            this.targetText = targetText;
            targetName = KStrings.lowercase(name());
        }

        public String getTargetText() {
            return targetText;
        }

        public String getTargetName() {
            return targetName;
        }

        public static Target fromTargetName(String targetName) {
            if(NAME_MAP.containsKey(targetName)) {
                return NAME_MAP.get(targetName);
            }
            throw new NoSuchElementException("Unknown target name: " + targetName);
        }

        private static Map<String, Target> buildNameMap() {
            return Stream.of(values()).collect(Collectors.toMap(Target::getTargetName, t -> t));
        }
    }
}
