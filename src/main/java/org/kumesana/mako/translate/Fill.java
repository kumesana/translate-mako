package org.kumesana.mako.translate;

import java.util.List;

public class Fill {

    public static void main(String[] args) {
        TranslationSet set = TranslationSet.load();

        System.out.format("Read translations file, on %d scripts%n", set.getNbScripts());

        for(ScriptLineSet script : set.getScripts()) {
            if(script.hasNonFilledLine()) {
                String scriptName = script.getName();
                List<String> scriptLines = Scripts.load(scriptName);

                for(Line line : script.getLines()) {
                    if(!line.hasOriginal()) {
                        StringBuilder builder = new StringBuilder();
                        for(int i = 0; i < line.getCount(); i++) {
                            if(i > 0) {
                                builder.append('\n');
                            }
                            builder.append(scriptLines.get(line.getNumber() + i - 1));
                        }
                        String text = builder.toString();
                        line.setOriginal(text);
                        line.setTranslation(text);
                    }
                }
            }
        }

        TranslationSet.write(set);

        System.out.format("Wrote translations file, with %d scripts%n", set.getNbScripts());
    }
}
