package org.kumesana.mako.translate;

import java.util.ArrayList;
import java.util.List;

public class Sequence {
    private final TranslationSet set;
    private final String file;
    private final List<Line> lines = new ArrayList<>();

    public Sequence(TranslationSet set, String file) {
        this.set = set;
        this.file = file;
    }

    public String getFile() {
        return file;
    }

    public List<Line> getLines() {
        return lines;
    }

    public LineBuilder buildNewLine(int number, int count) {
        return new LineBuilder(number, count);
    }

    public class LineBuilder {
        private final int number;
        private final int count;
        private String suffix = "";
        private String newlinePrefix = "\n";
        private String original;
        private final List<LineComponent> components = new ArrayList<>();

        private LineBuilder(int number, int count) {
            if (number < 1) {
                throw new IllegalArgumentException("Illegal line number: " + number);
            }
            if(count < 1) {
                throw new IllegalArgumentException("Illegal line count: " + count);
            }
            this.number = number;
            this.count = count;
        }

        public LineBuilder withSuffix(String suffix) {
            if(suffix == null) {
                suffix = "";
            }
            this.suffix = suffix;
            return this;
        }

        public LineBuilder withNewlinePrefix(String newlinePrefix) {
            if(newlinePrefix == null) {
                newlinePrefix = "\n";
            }
            this.newlinePrefix = newlinePrefix;
            return this;
        }

        public LineBuilder withOriginal(String original) {
            this.original = original;
            return this;
        }

        public void addTextComponent(String text) {
            components.add(new StringComponent(text));
        }

        /*public LineBuilder addAtomComponent(String name) {
            components.add(AtomComponent.fromAtomName(name));
            return this;
        }*/

        public void addLinkComponent(String target, String text) {
            components.add(LinkComponent.buildFrom(target, text));
        }

        public void addVarComponent(String name) {
            components.add(VarComponent.fromVarName(name));
        }

        public void addLine() {
            Line line = new Line(number, count, suffix, newlinePrefix, original, components);
            lines.add(line);
            set.addLine(file, line);
        }
    }
}
