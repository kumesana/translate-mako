package org.kumesana.mako.translate;

import org.kumesana.common.strings.KStrings;
import org.kumesana.mako.translate.xml.XmlInlineContentReceiver;

import java.util.HashMap;
import java.util.Map;

public enum AtomComponent implements LineComponent {
    R;

    private final String atomName;
    private final String builtString;

    private static final Map<String, AtomComponent> nameMap = buildNameMap();

    AtomComponent() {
        atomName = KStrings.lowercase(name());
        builtString = '[' + atomName + ']';
    }

    public String getAtomName() {
        return atomName;
    }

    @Override
    public String buildString() {
        return builtString;
    }

    @Override
    public void addToInlineReceiver(XmlInlineContentReceiver receiver) {
        receiver.addAtomElement(atomName);
    }

    private static Map<String, AtomComponent> buildNameMap() {
        Map<String, AtomComponent> map = new HashMap<>();
        for(AtomComponent component : values()) {
            map.put(component.atomName, component);
        }
        return map;
    }

    public static AtomComponent fromAtomName(String atomName) {
        if(!nameMap.containsKey(atomName)) {
            throw new IllegalArgumentException("Unknown atomic component: " + atomName);
        }
        return nameMap.get(atomName);
    }
}
