package org.kumesana.mako.translate;

import java.util.Collections;
import java.util.List;

public class Line implements Comparable<Line> {

    private final int number;
    private final int count;
    private final String suffix;
    private final String newlinePrefix;

    private String original;
    private List<LineComponent> translation;

    public Line(int number, int count, String suffix, String newlinePrefix, String original, List<LineComponent> translation) {
        if (number < 1) {
            throw new IllegalArgumentException("Illegal line number: " + number);
        }
        if(count < 1) {
            throw new IllegalArgumentException("Illegal line count: " + count);
        }
        this.number = number;
        this.count = count;
        this.suffix = suffix;
        this.newlinePrefix = newlinePrefix;
        this.original = original;
        this.translation = translation;
    }

    public int getNumber() {
        return number;
    }

    public int getCount() {
        return count;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getNewlinePrefix() {
        return newlinePrefix;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public List<LineComponent> getTranslation() {
        return translation;
    }

    public void setTranslation(String text) {
        this.translation = Collections.singletonList(new StringComponent(text));
    }

    public boolean hasOriginal() {
        return original != null;
    }

    @Override
    public int compareTo(Line oLine) {
        return Integer.compare(number, oLine.number);
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        } else if(o == this) {
            return true;
        } else if(o instanceof Line oLine) {
            return number == oLine.number;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(number).append(':').append(count);
        if(hasOriginal()) {
            builder
                    .append("\noriginal:\n")
                    .append(original)
                    .append("\ntranslation:\n")
                    .append(translation);
        }
        return builder.toString();
    }
}
