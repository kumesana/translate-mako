package org.kumesana.mako.translate;

import org.kumesana.common.strings.KStrings;
import org.kumesana.mako.translate.xml.XmlInlineContentReceiver;

import java.util.HashMap;
import java.util.Map;

public enum VarComponent implements LineComponent {
    NAME("[名前] [名字]"),
    AGE("[年齢]");

    private final String varName;
    private final String builtString;

    private static final Map<String, VarComponent> nameMap = buildNameMap();

    VarComponent(String builtString) {
        varName = KStrings.lowercase(name());
        this.builtString = builtString;
    }

    public String getVarName() {
        return varName;
    }

    @Override
    public String buildString() {
        return builtString;
    }

    @Override
    public void addToInlineReceiver(XmlInlineContentReceiver receiver) {
        receiver.addVariable(varName);
    }

    private static Map<String, VarComponent> buildNameMap() {
        Map<String, VarComponent> map = new HashMap<>();
        for(VarComponent component : values()) {
            map.put(component.varName, component);
        }
        return map;
    }

    public static VarComponent fromVarName(String varName) {
        if(!nameMap.containsKey(varName)) {
            throw new IllegalArgumentException("Unknown variable name: " + varName);
        }
        return nameMap.get(varName);
    }
}
