package org.kumesana.mako.translate;

import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.Text;
import org.kumesana.mako.translate.xml.XmlExport;
import org.kumesana.xml.LoadXml;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TranslationSet {

    public static final String XML_PATH_STRING = "site/translation.xml";
    public static final Path XML_PATH = Paths.get(XML_PATH_STRING);

    private final Map<String, ScriptLineSet> scripts = new TreeMap<>();
    private final List<Chapter> chapters = new ArrayList<>();

    public Collection<Chapter> getChapters() {
        return chapters;
    }

    public Chapter createChapter(String name) {
        Chapter chapter = new Chapter(this, name);
        chapters.add(chapter);
        return chapter;
    }

    public int getNbScripts() {
        return scripts.size();
    }

    public Collection<ScriptLineSet> getScripts() {
        return scripts.values();
    }

    void addLine(String scriptName, Line line) {
        ScriptLineSet script = scripts.computeIfAbsent(scriptName, ScriptLineSet::new);
        script.addLine(line);
    }

    @Override
    public String toString() {
        return chapters.toString();
    }

    public static TranslationSet load() {
        TranslationSet set = new TranslationSet();
        LoadXml
                .fromPath(XML_PATH)
                .getRootElement()
                .getChildren("chapter")
                .forEach(chapterElt -> {
                    Chapter chapter = set.createChapter(chapterElt.getAttributeValue("name"));

                    chapterElt
                            .getChildren("sequence")
                            .forEach(sequenceElt -> {
                                Sequence sequence = chapter.createSequence(sequenceElt.getAttributeValue("file"));

                                for(Element lineElt : sequenceElt.getChildren("line")) {
                                    int num = Integer.parseInt(lineElt.getAttributeValue("start"));
                                    String countString = lineElt.getAttributeValue("count");
                                    int count;
                                    if(countString == null) {
                                        count = 1;
                                    } else {
                                        count = Integer.parseInt(countString);
                                    }

                                    var builder = sequence
                                            .buildNewLine(num, count)
                                            .withSuffix(lineElt.getAttributeValue("suffix"))
                                            .withNewlinePrefix(lineElt.getAttributeValue("newlines"))
                                            .withOriginal(lineElt.getChildText("original"));

                                    Element translationElt = lineElt.getChild("translation");
                                    if(translationElt != null) {
                                        for(Content content : translationElt.getContent()) {
                                            if(content instanceof Text text) {
                                                builder.addTextComponent(text.getText());
                                            } else if(content instanceof Element componentElt) {
                                                switch (componentElt.getName()) {
                                                    case "link" -> {
                                                        String target = componentElt.getAttributeValue("to");
                                                        builder.addLinkComponent(target, componentElt.getText());
                                                    }
                                                    case "var" -> {
                                                        String varName = componentElt.getAttributeValue("n");
                                                        builder.addVarComponent(varName);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    builder.addLine();
                                }
                            });
                });

        return set;
    }

    public static void write(TranslationSet set) {
        XmlExport.writeDocument(XML_PATH, "texts", root -> {

            for(Chapter chapter : set.getChapters()) {
                root.addElement("chapter")
                        .withAttribute("name", chapter.getName())
                        .withContent(chapterGen -> {

                            for(Sequence sequence : chapter.getSequences()) {
                                chapterGen.addElement("sequence")
                                        .withAttribute("file", sequence.getFile())
                                        .withContent(sequenceGen -> {

                                            for(Line line : sequence.getLines()) {

                                                var builder = sequenceGen.addElement("line")
                                                        .withAttribute("start", String.valueOf(line.getNumber()));

                                                if(line.getCount() > 1) {
                                                    builder.withAttribute("count", String.valueOf(line.getCount()));
                                                }
                                                if(!line.getSuffix().isEmpty()) {
                                                    builder.withAttribute("suffix", line.getSuffix());
                                                }
                                                if(!line.getNewlinePrefix().equals("\n")) {
                                                    builder.withAttribute("newlines", line.getNewlinePrefix());
                                                }

                                                builder.withContent(lineGen -> {

                                                    if(line.hasOriginal()) {
                                                        lineGen.addInlineElement("original", line.getOriginal());

                                                        lineGen.addInlineElement("translation", trGen -> {

                                                            for(LineComponent component : line.getTranslation()) {
                                                                component.addToInlineReceiver(trGen);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                            }
                        });
            }
        });
    }
}
