package org.kumesana.mako.translate;

import java.util.ArrayList;
import java.util.List;

public class Chapter {
    private final TranslationSet set;
    private final String name;
    private final List<Sequence> sequences = new ArrayList<>();

    public Chapter(TranslationSet set, String name) {
        this.set = set;
        this.name = name;
    }

    public Sequence createSequence(String file) {
        Sequence sequence = new Sequence(set, file);
        sequences.add(sequence);
        return sequence;
    }

    public String getName() {
        return name;
    }

    public List<Sequence> getSequences() {
        return sequences;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("chapter ").append(name);
        for(Sequence sequence : sequences) {
            builder.append('\n').append(sequence);
        }
        return builder.toString();
    }
}
