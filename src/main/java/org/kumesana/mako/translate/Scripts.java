package org.kumesana.mako.translate;

import org.kumesana.common.files.KFiles;
import org.kumesana.common.iostreams.KWriter;
import org.kumesana.env.KumeEnv;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;

public final class Scripts {

    public static final Path SCRIPT_FOLDER = KumeEnv.getGamesDirectory().resolve("touch-mako/Mako Translation/data/patch/scenario");
    public static final Path PATCH_FOLDER = KumeEnv.getGamesDirectory().resolve("touch-mako/Mako Translation/data/patch/scenario_english");

    public static List<String> load(String name) {
        Path file = SCRIPT_FOLDER.resolve(name);
        return KFiles.readAllLines(file, StandardCharsets.UTF_16LE);
    }

    public static KWriter writerFor(String name) {
        Path file = PATCH_FOLDER.resolve(name);
        return KFiles.newWriter(file, StandardCharsets.UTF_16LE);
    }

    /** Prevents instantiation. */
    private Scripts() { }
}
