package org.kumesana.mako.translate;

import org.kumesana.mako.translate.xml.XmlInlineContentReceiver;

public class StringComponent implements LineComponent {

    private final String text;

    public StringComponent(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String buildString() {
        return text;
    }

    @Override
    public void addToInlineReceiver(XmlInlineContentReceiver receiver) {
        receiver.addText(text);
    }
}
