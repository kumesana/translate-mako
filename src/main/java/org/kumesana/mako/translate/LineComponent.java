package org.kumesana.mako.translate;

import org.kumesana.mako.translate.xml.XmlInlineContentReceiver;

public interface LineComponent {
    String buildString();
    void addToInlineReceiver(XmlInlineContentReceiver receiver);
}
