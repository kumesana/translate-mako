package org.kumesana.mako.translate;

import org.kumesana.common.iostreams.KWriter;

import java.util.Iterator;

public class Apply {
    public static void main(String[] args) {
        TranslationSet set = TranslationSet.load();

        System.out.format("Read translations file, on %d scripts%n", set.getNbScripts());
        int count = 0;

        for(ScriptLineSet script : set.getScripts()) {
            String scriptName = script.getName();
            System.out.println("Rewriting script " + scriptName);

            Iterator<String> originalLines = Scripts.load(scriptName).iterator();
            Iterator<Line> translatedLines = script.getLines().iterator();

            try(KWriter writer = Scripts.writerFor(scriptName)) {
                int lineNum = 0;
                Line nextTranslatedLine = translatedLines.next();

                while(originalLines.hasNext()) {
                    String originalLine = originalLines.next();
                    lineNum++;

                    if(nextTranslatedLine != null && lineNum == nextTranslatedLine.getNumber()) {
                        for(int i = 0; i < nextTranslatedLine.getCount() - 1; i++) {
                            originalLines.next();
                            lineNum++;
                        }
                        writer.write(getScriptText(nextTranslatedLine));
                        if(translatedLines.hasNext()) {
                            nextTranslatedLine = translatedLines.next();
                        } else {
                            nextTranslatedLine = null;
                        }
                    } else {
                        writer.write(originalLine);
                    }
                    writer.write("\r\n");
                }
            }

            count++;
        }

        System.out.println("Rewrote " + count + " files");
    }

    private static String getScriptText(Line line) {
        StringBuilder builder = new StringBuilder();
        for(LineComponent component : line.getTranslation()) {
            builder.append(component.buildString().replace("\n", line.getNewlinePrefix().replace("\n", "\r\n")));
        }
        builder.append(line.getSuffix());

        return builder.toString();
    }
}
