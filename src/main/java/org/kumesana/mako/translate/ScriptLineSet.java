package org.kumesana.mako.translate;

import java.util.Collection;
import java.util.Collections;
import java.util.TreeSet;

public class ScriptLineSet {
    private final String name;
    private final TreeSet<Line> lines = new TreeSet<>();

    public ScriptLineSet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Collection<Line> getLines() {
        return lines;
    }

    void addLine(Line line) {
        if(lines.contains(line)) {
            throw new IllegalArgumentException("Script " + name + " line " + line.getNumber() + " was already referenced");
        }
        lines.add(line);
    }

    public boolean hasNonFilledLine() {
        return lines.stream().anyMatch(line -> !line.hasOriginal());
    }

    @Override
    public String toString() {
        return Collections.singletonMap(name, lines).toString();
    }
}
