package org.kumesana.mako.translate.xml;

import org.kumesana.common.files.KFiles;

import java.nio.file.Path;
import java.util.function.Consumer;

public final class XmlExport {

    public static void writeDocument(Path documentPath, String root, Consumer<XmlContentReceiver> generator) {
        KFiles.useWriter(documentPath, writer -> {
            writer.write("""
                    <?xml version="1.0" encoding="utf-8"?>""" + "\n");
            XmlContentReceiver baseReceiver = new StandardContentReceiver(writer, 0);
            baseReceiver.addElement(root, generator);
        });
    }

    /** Prevents instantiation. */
    private XmlExport() { }
}
