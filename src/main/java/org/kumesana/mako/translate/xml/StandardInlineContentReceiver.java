package org.kumesana.mako.translate.xml;

import org.kumesana.common.iostreams.KWriter;

class StandardInlineContentReceiver extends AbstractContentReceiver implements XmlInlineContentReceiver {

    StandardInlineContentReceiver(KWriter writer, int indent) {
        super(writer, indent);
    }

    @Override
    public void addText(String text) {
        writer.write(escapeText(text));
    }

    @Override
    public void addAtomElement(String name) {
        writer.write("<" + name +"/>");
    }

    @Override
    public void addLink(String target, String text) {
        writer.write("<link to=\"" + target + "\">");
        writer.write(escapeText(text));
        writer.write("</link>");
    }

    @Override
    public void addVariable(String name) {
        writer.write("<var n=\"" + name + "\"/>");
    }
}
