package org.kumesana.mako.translate.xml;

import org.kumesana.common.iostreams.KWriter;

import java.util.function.Consumer;

class StandardContentReceiver extends AbstractContentReceiver implements XmlContentReceiver {

    StandardContentReceiver(KWriter writer, int indent) {
        super(writer, indent);
    }

    @Override
    public ElementAttributesBuilder addElement(String name) {
        return new StandardElementAttributesBuilder(writer, indent, name);
    }

    @Override
    public void addInlineElement(String name, Consumer<XmlInlineContentReceiver> generator) {
        indent();
        writeStartTag(name);
        generator.accept(new StandardInlineContentReceiver(writer, indent + 1));
        writeEndTag(name);
        writer.write('\n');
    }
}
