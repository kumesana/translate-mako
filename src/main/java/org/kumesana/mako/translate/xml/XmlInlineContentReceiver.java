package org.kumesana.mako.translate.xml;

public interface XmlInlineContentReceiver {
    void addText(String text);
    void addAtomElement(String name);
    void addLink(String target, String text);
    void addVariable(String name);
}
