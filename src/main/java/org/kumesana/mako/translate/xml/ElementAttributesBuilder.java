package org.kumesana.mako.translate.xml;

import java.util.function.Consumer;

public interface ElementAttributesBuilder {
    ElementAttributesBuilder withAttribute(String name, String value);
    void withContent(Consumer<XmlContentReceiver> generator);
}
