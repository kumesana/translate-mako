package org.kumesana.mako.translate.xml;

import org.kumesana.common.iostreams.KWriter;

import java.util.HashMap;
import java.util.Map;

class AbstractContentReceiver {
    private static final Map<Character, String> TEXT_CONTENT_ESCAPE = Map.of('&', "amp", '<', "lt");
    private static final Map<Character, String> ATTRIBUTE_ESCAPE = buildAttributeEscape();
    protected final KWriter writer;
    protected final int indent;

    AbstractContentReceiver(KWriter writer, int indent) {
        this.writer = writer;
        this.indent = indent;
    }

    protected void writeStartTag(String name) {
        writer.write("<" + name + ">");
    }

    protected void writeEndTag(String name) {
        writer.write("</" + name + ">");
    }

    protected void indent() {
        for(int i = 0; i < 2 * indent; i++) {
            writer.write(' ');
        }
    }

    static String escapeText(String text) {
        return escapeText(text, TEXT_CONTENT_ESCAPE);
    }

    static String escapeAttributeText(String text) {
        return escapeText(text, ATTRIBUTE_ESCAPE);
    }

    private static String escapeText(String text, Map<Character, String> escapeMap) {
        StringBuilder builder = new StringBuilder();
        text.codePoints().forEach(c -> {
            char ch = (char)c;
            if(escapeMap.containsKey(ch)) {
                builder.append('&').append(escapeMap.get(ch)).append(';');
            } else {
                builder.append(ch);
            }
        });
        return builder.toString();
    }

    private static Map<Character, String> buildAttributeEscape() {
        Map<Character, String> map = new HashMap<>(TEXT_CONTENT_ESCAPE);
        map.put('"', "quot");
        map.put('\n', "#xA");
        return map;
    }
}
