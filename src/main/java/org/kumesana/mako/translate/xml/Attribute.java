package org.kumesana.mako.translate.xml;

record Attribute(String name, String value) { }
