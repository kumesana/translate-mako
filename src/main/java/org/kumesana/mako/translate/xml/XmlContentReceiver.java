package org.kumesana.mako.translate.xml;

import java.util.function.Consumer;

public interface XmlContentReceiver {
    ElementAttributesBuilder addElement(String name);

    default void addElement(String name, Consumer<XmlContentReceiver> generator) {
        addElement(name).withContent(generator);
    }

    void addInlineElement(String name, Consumer<XmlInlineContentReceiver> generator);

    default void addInlineElement(String name, String content) {
        addInlineElement(name, g -> g.addText(content));
    }
}
