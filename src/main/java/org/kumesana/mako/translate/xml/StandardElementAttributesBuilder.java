package org.kumesana.mako.translate.xml;

import org.kumesana.common.iostreams.KWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

class StandardElementAttributesBuilder extends AbstractContentReceiver implements ElementAttributesBuilder {

    private final String name;
    private final List<Attribute> attributes = new ArrayList<>();

    StandardElementAttributesBuilder(KWriter writer, int indent, String name) {
        super(writer, indent);
        this.name = name;
    }

    @Override
    public ElementAttributesBuilder withAttribute(String name, String value) {
        attributes.add(new Attribute(name, value));
        return this;
    }

    @Override
    public void withContent(Consumer<XmlContentReceiver> generator) {
        writeStartTag();
        generator.accept(new StandardContentReceiver(writer, indent + 1));
        writeEndTag();
    }

    private void writeStartTag() {
        indent();
        writer.write("<" + name);
        if(!attributes.isEmpty()) {
            for(Attribute attribute : attributes) {
                writer.write(" " + attribute.name() + "=\"" + escapeAttributeText(attribute.value()) + '"');
            }
        }
        writer.write(">\n");
    }

    private void writeEndTag() {
        indent();
        writer.write("</" + name + ">\n");
    }
}
