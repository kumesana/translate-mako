module mako.translate {
    requires org.kumesana.commons;
    requires org.kumesana.env;
    requires transitive org.kumesana.xml;
}
